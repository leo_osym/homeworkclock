﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeworkClock
{
    public partial class ClockForm : Form
    {
        private float[] _sin = new float[360];
        private float[] _cos = new float[360];
        public ClockForm()
        {
            for (int i = 0; i < 360; ++i)
            {
                _sin[i] = (float)Math.Sin((i-90) * 2 *Math.PI / 360.0);
                _cos[i] = (float)Math.Cos((i-90) * 2* Math.PI / 360.0);
            }
            InitializeComponent();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void ClockForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            int clockSize = (ClientSize.Width > ClientSize.Height ?
                            ClientSize.Height / 2 : ClientSize.Width / 2);
            Point center = new Point(ClientSize.Width / 2, ClientSize.Height / 2);
            gr.DrawString("Press <Esc> to exit", new Font("Arial", 10), new SolidBrush(Color.Black), 85, 95);
            DrawArrows(gr, center, clockSize);
            DrawEdging(gr, center, clockSize);
        }

        #region DrawEdging
        /// <summary>
        /// Method which draws edging for our clock
        /// </summary>
        private void DrawEdging(Graphics gr, Point center, int Size)
        {
            
            int clockSize = Size - 20;
            float c1radius = clockSize;
            float c2radius = clockSize + 5;
            float c3radius = Size;
            Pen pen;
            Pen penUsual = new Pen(Color.Black, 1.5f);
            Pen penBold = new Pen(Color.Black, 3f);
            gr.DrawEllipse(penUsual, center.X - c1radius, center.Y - c1radius, 2 * c1radius, 2 * c1radius);
            gr.DrawEllipse(penUsual, center.X - c2radius, center.Y - c2radius, 2 * c2radius, 2 * c2radius);
            gr.DrawEllipse(penUsual, center.X - c3radius, center.Y - c3radius, 2 * c3radius, 2 * c3radius);
            Pen p = new Pen(Color.Black, 2f);
            for (int angle = 0, i = 0; i < 60; i++)
            {
                angle = i * 360 / 60;
                if (i % 5 == 0)
                {
                    pen = penBold;
                    DrawNumbers(gr, center, c2radius, i);
                }
                else pen = penUsual;
                gr.DrawLine(pen,
                    new PointF
                    {
                        X = center.X + c1radius * _cos[angle % 360],
                        Y = center.Y + c1radius * _sin[angle % 360],
                    },
                    new PointF
                    {
                        X = center.X + c2radius * _cos[angle % 360],
                        Y = center.Y + c2radius * _sin[angle % 360],
                    });
            }
        }
        #endregion

        #region DrawNumbers
        /// <summary>
        /// Draw numbers to edging of our clock
        /// </summary>
        private void DrawNumbers(Graphics gr, Point center, float radius, int i)
        {
            int angle = i * 360 / 60;
            Font drawFont = new Font("Lucida Calligraphy", 14);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
            gr.DrawString((i > 0 ? i / 5 : 12).ToString(), drawFont, drawBrush,
                            center.X + (radius + 8) * _cos[angle % 360],
                            center.Y + (radius + 8) * _sin[angle % 360],
                            format);
        }
        #endregion

        #region DrawArrows
        /// <summary>
        /// Draw arrows
        /// </summary>
        private void DrawArrows(Graphics gr, Point center, int clockSize)
        {
            int ArrowLength = clockSize - 25;
            int ArrowLengthMin = ArrowLength - 30;
            int ArrowLengthHour = ArrowLength - 50;

            int angleSeconds = DateTime.Now.Second * 360 / 60;
            int angleMinutes = DateTime.Now.Minute * 360 / 60;
            int angleHours = DateTime.Now.Hour * 360 / 12;

            gr.DrawLine(new Pen(Color.Red, 1f),
                center,
                new PointF
                {
                    X = center.X + ArrowLength * _cos[angleSeconds % 360],
                    Y = center.Y + ArrowLength * _sin[angleSeconds % 360],
                });
            gr.DrawLine(new Pen(Color.Green, 2f),
                center,
                new PointF
                {
                    X = center.X + ArrowLengthMin * _cos[angleMinutes % 360],
                    Y = center.Y + ArrowLengthMin * _sin[angleMinutes % 360],
                });
            gr.DrawLine(new Pen(Color.Blue, 3f),
                center,
                new PointF
                {
                    X = center.X + ArrowLengthHour * _cos[angleHours % 360],
                    Y = center.Y + ArrowLengthHour * _sin[angleHours % 360],
                });
        }
        #endregion

        #region MoveForm

        private bool isMouseDown = false;
        private Point leftTopPoint;
        private void ClockForm_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            leftTopPoint.X = e.X;
            leftTopPoint.Y = e.Y;
        }

        private void ClockForm_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void ClockForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                this.Location =
                    new Point(
                        Location.X + e.X - leftTopPoint.X,
                        Location.Y + e.Y - leftTopPoint.Y
                    );
            }
        }
        #endregion

        #region CloseForm
        private void ClockForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
        #endregion
    }
}
